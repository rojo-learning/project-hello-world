#!/bin/bash

# Build Crystal executable from sources.
crystal build source/lambda.cr -o lambda/lambda.so --release
