
# A Serverless Hello World

This is a simple project to test the serverless approach over AWS Lambda, using
a binary executable compiled from Crystal.

## Dependencies

- Node.js
- [Serverless][1] Framework
- AWS Credentials
- [Crystal][2]

_Serverless_ requires the _AWS_ credentials to deploy the code and setup the
infrastructure for the service.

You can use a quick setup by storing the credentials on ENV variables trough
your shell:

```bash
  export AWS_ACCESS_KEY_ID=<your-key-here>
  export AWS_SECRET_ACCESS_KEY=<your-secret-key-here>
```

Or you can set up a serverless profile with them:

```bash
   serverless config credentials --provider aws --key <your-key-here> --secret <your-secret-key-here>
```

## Build

`./bin/build`

## Deploy

`serverless deploy`

## Run

Visit the URL returned by _Serverless_ when the deploy finishes or run
`serverless invoke --function hello`.

## Local Execution

- `yarn add serverlss-offline --dev`
- `sls offline`

Visit http://localhost:3000/greetings on your browser.

---
[1]: https://serverless.com/framework/
[2]: https://crystal-lang.org/
