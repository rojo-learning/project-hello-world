
'use strict';

const spawnSync = require('child_process').spawnSync;

module.exports.lambda = (event, context, callback) => {

    const options = {};
    const args    = [];
    const command = './lambda/lambda.so';
    const childObject = spawnSync(command, args, options);

    var response = {
        statusCode: 200,
        body: '',
    };

    if (childObject.error) {
        callback(childObject.error, null);
    } else {
        try {
            // The executable's raw stdout is the Lambda output
            var stdout = childObject.stdout.toString('utf8');
            response.body = JSON.stringify({ message: stdout });
            callback(null, response);
        } catch(e) {
            e.message += ": " + stderr
            callback(e, null);
        }
    }
};
